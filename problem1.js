

function car33(inventory) {
    for (car of inventory) {
        id = car["id"]
        year = car["car_year"]
        car_make = car["car_make"]
        car_model = car["car_model"]
        if (id == 33) {
            text = "car " + id + " is a " + year + " " + car_make + " " + car_model
            return text
        }
    }
}

module.exports = car33