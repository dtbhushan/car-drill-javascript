const carYear = require("./problem4.js")

function oldCars(inventory){
    oldCarsCount = 0
    for(year of carYear(inventory)){
        if ( year < 2000){
            oldCarsCount += 1
        }
    }
    return oldCarsCount
}

module.exports = oldCars