
function onlyBMWAndAudi(inventory){
    BMWAndAudi = []
    for(car of inventory){
        if (car["car_make"] == "BMW" || car["car_make"] == "Audi"){
            BMWAndAudi.push(car)
        }
    }
    json = JSON.stringify(BMWAndAudi)
    return json
}

module.exports = onlyBMWAndAudi